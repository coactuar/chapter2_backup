-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 12:10 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casediscussion`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021/05/15 11:00:25', '2021-05-15', '2021-05-15', 1, 'Abbott'),
(2, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021/05/15 11:04:57', '2021-05-15', '2021-05-15', 1, 'Abbott'),
(3, 'DR KESHAVA R', 'drkeshavar@gmail.com', 'Bangalore', 'fortis', NULL, NULL, '2021/05/18 18:12:41', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(4, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/18 18:20:17', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(5, 'ARUN SRINIVAS', 'arunsrinivas18@gmail.com ', 'MYSORE', 'APOLLO BGS HOSPITALS', NULL, NULL, '2021/05/18 18:40:50', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(6, 'Senthil Kumar Natarajan', 'senthil.natarajan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/05/18 18:43:16', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(7, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/18 18:46:03', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(8, 'Dr Karthik Vsudevan', 'karvasudevan@gmail.com', 'Bangalore', 'ColumbiaAsia Hospital', NULL, NULL, '2021/05/18 18:51:50', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(9, 'Dr Sanjay Mehrotra', 'sanjaym.dr@gmail.com', 'Bangalore', 'NH Institute of Medical Sciences', NULL, NULL, '2021/05/18 18:53:33', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(10, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/18 18:56:39', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(11, 'Dr P K Sahoo', 'drprasant_s@apollohospitals.com', 'Bhubaneswar', 'Apollo Hospital', NULL, NULL, '2021/05/18 18:57:16', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(12, 'Dr AJ Swamy', 'ajayswamy@rediffmail.com', 'Bangalore', 'command hospital', NULL, NULL, '2021/05/18 18:57:22', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(13, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/18 18:59:26', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(14, 'V S PRAKASH', 'drprakashvs@gmail.com', 'BANGALORE', 'RAMAIAH NARAYANA HEART CENTRE', NULL, NULL, '2021/05/18 19:00:30', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(15, 'Senthil Kumar Natarajan', 'senthil.natarajan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/05/18 19:00:40', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(16, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/18 19:00:57', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(17, 'Deepak Davidson', 'deepakdavidson@yahoo.com', 'Kottayam', 'Caritas Hospital', NULL, NULL, '2021/05/18 19:43:32', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(18, 'Deepak Davidson', 'deepakdavidson@yahoo.com', 'Kottayam', 'Caritas Hospital', NULL, NULL, '2021/05/18 19:51:23', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(19, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Hosur', 'ABBOTT', NULL, NULL, '2021/05/18 20:28:48', '2021-05-18', '2021-05-18', 1, 'Abbott'),
(20, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/05/18 20:32:16', '2021-05-18', '2021-05-18', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Covered stent to seal perf ? ', '2021-05-18 19:19:15', 'Abbott', 0, 0),
(2, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'How many pulses  delivered', '2021-05-18 19:23:36', 'Abbott', 0, 0),
(3, 'Sreevatsa N S', 'nadig33@gmail.com', 'When grafts are patent, Whats the need for intervention? Was a stress mpi performed?', '2021-05-18 19:55:32', 'Abbott', 0, 0),
(4, 'Dr Sunil Kumar S', 'sunilbmc98@gmail.com', 'Orbital atherectomy might help in a better way for eccentric calcium and calcium nodule than rotablation and ivl. Hope we see it in India very soon', '2021-05-18 20:05:14', 'Abbott', 0, 0),
(5, 'Sreevatsa N S', 'nadig33@gmail.com', 'Superb cases dr Deepak ðŸ‘ŒðŸ‘Œ', '2021-05-18 20:43:07', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-05-15 11:01:37', '2021-05-15 11:01:37', '2021-05-15 12:31:37', 0, 'Abbott', 'b157c3a728f725eb6777be3cb2ea8212'),
(2, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-05-15 11:04:27', '2021-05-15 11:04:27', '2021-05-15 12:34:27', 0, 'Abbott', '08ee794518136d43094063f8a3aba693'),
(3, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-05-17 14:01:29', '2021-05-17 14:01:29', '2021-05-17 15:31:29', 0, 'Abbott', '8be743a07c8186894127d06e7ae0f54e'),
(4, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-05-17 20:13:53', '2021-05-17 20:13:53', '2021-05-17 21:43:53', 0, 'Abbott', '6d9f97a12aa135a05f460576f334ae2d'),
(5, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-17 20:39:43', '2021-05-17 20:39:43', '2021-05-17 22:09:43', 0, 'Abbott', 'ba164eddbcc1bfd298b63b8db814517b'),
(6, 'Udit ', 'udit_m.in@jtbap.com', 'Gurgaon', 'Test', NULL, NULL, '2021-05-17 20:50:35', '2021-05-17 20:50:35', '2021-05-17 20:50:49', 0, 'Abbott', '0cb80f1161524a096358355d4021b995'),
(7, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-17 22:01:14', '2021-05-17 22:01:14', '2021-05-17 23:31:14', 0, 'Abbott', '0a8d0bbd8280566af7af3569c30baa44'),
(8, 'Udit', 'udit_m.in@jtbap.com', 'Gurgaon', 'Test', NULL, NULL, '2021-05-17 22:16:52', '2021-05-17 22:16:52', '2021-05-17 23:46:52', 0, 'Abbott', '7a1f0cad60511c815012a907112e70cb'),
(9, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-18 09:38:04', '2021-05-18 09:38:04', '2021-05-18 11:08:04', 0, 'Abbott', '42de1bc0fd53ebfcf59acc63a6986dc5'),
(10, 'Udit', 'udit_m.in@jtbap.com', 'Gurgaon', 'Test', NULL, NULL, '2021-05-18 09:45:12', '2021-05-18 09:45:12', '2021-05-18 09:45:28', 0, 'Abbott', '894cdd925c9d78ea63d78b8beb1f9344'),
(11, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-18 09:49:09', '2021-05-18 09:49:09', '2021-05-18 11:19:09', 0, 'Abbott', 'f2214881683b3b50a717501925c0263c'),
(12, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-18 09:58:52', '2021-05-18 09:58:52', '2021-05-18 10:00:31', 0, 'Abbott', 'ba7a35eaaaab64a1614c0d104205fe70'),
(13, 'santhosh kumar singh b', 'santhoshsingh78@yahoo.co.in', 'BANGALORE', 'fortis hospital', NULL, NULL, '2021-05-18 10:06:16', '2021-05-18 10:06:16', '2021-05-18 11:36:16', 0, 'Abbott', 'c1bf855d539ee1b47120d63df5d19bd9'),
(14, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-18 17:36:37', '2021-05-18 17:36:37', '2021-05-18 19:06:37', 0, 'Abbott', '8fbae9f61584ef1ad175f73074e021b9'),
(15, 'Amal Vyas', 'amal.vyas@abbott.com', 'Abbott', 'Mumbai', NULL, NULL, '2021-05-18 18:36:55', '2021-05-18 18:36:55', '2021-05-18 20:06:55', 1, 'Abbott', '1fdd65ea15ef09636240a276dcc843dd'),
(16, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021-05-18 18:40:02', '2021-05-18 18:40:02', '2021-05-18 20:10:02', 1, 'Abbott', 'cc1d4cdf3c74cb8c8ac12aed8492d9b0'),
(17, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-18 18:46:54', '2021-05-18 18:46:54', '2021-05-18 19:14:39', 0, 'Abbott', '9790c24cac2684d794e2c985567b46ea'),
(18, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-18 18:52:22', '2021-05-18 18:52:22', '2021-05-18 20:22:22', 1, 'Abbott', 'b3a607062170d59cf000b958ca58106a'),
(19, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-18 18:53:31', '2021-05-18 18:53:31', '2021-05-18 20:23:31', 1, 'Abbott', '054fae7f6039ba8588ebc199d597d96e'),
(20, 'dr sarat kumae sahoo', 'drsaratsahoo@rediffmail.com', 'bhubaneswar', 'sumum', NULL, NULL, '2021-05-18 18:55:28', '2021-05-18 18:55:28', '2021-05-18 20:25:28', 1, 'Abbott', 'da0fa808bc970cbe4d2d53fbd0f72b63'),
(21, 'Dr.Anand N.B', 'anandnb55@gmail.com', 'Mysore ', 'Apollo Hospital ', NULL, NULL, '2021-05-18 18:58:17', '2021-05-18 18:58:17', '2021-05-18 20:28:17', 1, 'Abbott', 'fffbcea3ea2aad009879f6c759c97ef7'),
(22, 'Adhil', 'adhil071989@gmail.com', 'Mysuru', 'Apollo-BGS hospital ', NULL, NULL, '2021-05-18 18:58:41', '2021-05-18 18:58:41', '2021-05-18 20:28:41', 1, 'Abbott', '2cb929c2362b266dca16746d10be0874'),
(23, 'Abhilash ', 'abhilash.mohanan@abbott.com', 'Bangalore ', 'Abbott', NULL, NULL, '2021-05-18 18:59:01', '2021-05-18 18:59:01', '2021-05-18 20:29:01', 1, 'Abbott', 'ab38cd3ed4d91e6377fb6a587f5018c5'),
(24, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-18 18:59:55', '2021-05-18 18:59:55', '2021-05-18 20:29:55', 1, 'Abbott', '6a4a28cf47a31c967775110f03b46d84'),
(25, 'Sunil Roy', 'sunil.roy@abbott.com', 'bbsr', 'Av', NULL, NULL, '2021-05-18 19:00:28', '2021-05-18 19:00:28', '2021-05-18 20:30:28', 1, 'Abbott', '0b8967ffa5fd27f7a40b8ec218ee1af5'),
(26, 'Argha Dasmahapatra', 'dasmahapatraargha@gmail.com', 'Bhubaneswar', 'IMS & SUM Hospital', NULL, NULL, '2021-05-18 19:01:15', '2021-05-18 19:01:15', '2021-05-18 20:31:15', 1, 'Abbott', 'f5711b8c4b66b0f7b9be88b9a266ca33'),
(27, 'Sreevatsa N S', 'nadig33@gmail.com', 'Shimoga ', 'Nh', NULL, NULL, '2021-05-18 19:02:07', '2021-05-18 19:02:07', '2021-05-18 20:32:07', 1, 'Abbott', 'beb6d0372b73a526f8aedf9e152a312f'),
(28, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-18 19:02:11', '2021-05-18 19:02:11', '2021-05-18 20:32:11', 1, 'Abbott', 'fbbacf579186d85d8860b1e4726ca123'),
(29, 'Argha Dasmahapatra', 'dasmahapatraargha@gmail.com', 'Bhubaneswar', 'IMS & SUM Hospital', NULL, NULL, '2021-05-18 19:02:13', '2021-05-18 19:02:13', '2021-05-18 20:32:13', 1, 'Abbott', '229957e8b9cf8ae54c81d01b81f0a6db'),
(30, 'Chirag D', 'chiragnichi@gmail.com', 'Bangalore ', 'Fortis', NULL, NULL, '2021-05-18 19:02:24', '2021-05-18 19:02:24', '2021-05-18 20:32:24', 1, 'Abbott', '1bdbef3db75680896c00a21ee2bf2be6'),
(31, 'Sridhar L', 'srisrisastry@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-18 19:02:59', '2021-05-18 19:02:59', '2021-05-18 20:32:59', 1, 'Abbott', 'b4a686c9fdd34c61b27140c2ba75256e'),
(32, 'dr sarat kumae sahoo', 'drsaratsahoo@rediffmail.com', 'bhubaneswar', 'sumum', NULL, NULL, '2021-05-18 19:03:12', '2021-05-18 19:03:12', '2021-05-18 20:33:12', 1, 'Abbott', 'b90c91bc72f8a74cd9b8d04297769242'),
(33, 'Praveen', 'pjshetty1974@rediffmail.com', 'Mangalore', 'A J Hospital', NULL, NULL, '2021-05-18 19:03:15', '2021-05-18 19:03:15', '2021-05-18 20:33:15', 1, 'Abbott', '279f6784e4d0760f76fb3067dc65e6f2'),
(34, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:04:27', '2021-05-18 19:04:27', '2021-05-18 20:34:27', 1, 'Abbott', '45a0c27669de2836dbbc64d9849312a3'),
(35, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-05-18 19:04:36', '2021-05-18 19:04:36', '2021-05-18 20:34:36', 1, 'Abbott', 'c4d85dbaacd30f2c4c1bebced5d6ca20'),
(36, 'Susanta Pradhan ', 'susantapradhan77@gmail.com', 'Bhubaneswar ', 'Utkal Hospital ', NULL, NULL, '2021-05-18 19:04:44', '2021-05-18 19:04:44', '2021-05-18 20:34:44', 1, 'Abbott', '539177cf01abb086b7272d82bf8b0ea0'),
(37, 'madakari', '2020madhu.icare@gmail.com', 'Bengaluru', 'Sparsh', NULL, NULL, '2021-05-18 19:05:27', '2021-05-18 19:05:27', '2021-05-18 20:35:27', 1, 'Abbott', 'b10d38e7f2ffc404b3a7e22ba04d1b78'),
(38, 'DR ARTI GOVINDLAL BAHETI ', 'drartibaheti@gmail.com', 'Banglore ', 'Fortis', NULL, NULL, '2021-05-18 19:05:47', '2021-05-18 19:05:47', '2021-05-18 20:35:47', 1, 'Abbott', '13a94cb73d9db5282a6ec1b7d84b5e8e'),
(39, 'Sridhara G', 'sridharag@hotmail.com', 'Bangalore ', 'Vikram', NULL, NULL, '2021-05-18 19:05:51', '2021-05-18 19:05:51', '2021-05-18 20:35:51', 1, 'Abbott', 'b488fa4b6d730145e7843a2cbf30399e'),
(40, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-05-18 19:05:53', '2021-05-18 19:05:53', '2021-05-18 20:35:53', 1, 'Abbott', '207bd8b8b55730ec8599480df41fb3de'),
(41, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-18 19:06:21', '2021-05-18 19:06:21', '2021-05-18 20:36:21', 1, 'Abbott', '8fa7ef7fa0051e23beae9ba7a2582f9f'),
(42, 'Dr Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-05-18 19:06:23', '2021-05-18 19:06:23', '2021-05-18 20:48:12', 0, 'Abbott', '21ee33f2844145cd782b2a7697804d8f'),
(43, 'Praveen', 'pjshetty1974@rediffmail.com', 'Mangalore', 'A J Hospital', NULL, NULL, '2021-05-18 19:06:44', '2021-05-18 19:06:44', '2021-05-18 20:36:44', 1, 'Abbott', '91d03dc9255d38f65997f07ef1bebaaf'),
(44, 'Argha Dasmahapatra', 'dasmahapatraargha@gmail.com', 'Bhubaneswar', 'IMS & SUM Hospital', NULL, NULL, '2021-05-18 19:08:23', '2021-05-18 19:08:23', '2021-05-18 19:09:19', 0, 'Abbott', 'f922b68501b8dd6b3022d62a2c8bc80c'),
(45, 'Satya Prakash Sahoo', 'satyaprakashcathlab@gmail.com', 'Bhubaneswar', 'Sum ultimate Medicare', NULL, NULL, '2021-05-18 19:08:33', '2021-05-18 19:08:33', '2021-05-18 19:10:03', 0, 'Abbott', '9ab60c5e12186a6a60d18ea1fe7953ca'),
(46, 'Lijo ', 'lijo.k@abbott.con', 'Cochin ', 'Abbott. ', NULL, NULL, '2021-05-18 19:08:53', '2021-05-18 19:08:53', '2021-05-18 20:38:53', 1, 'Abbott', '8a4b986dddb09c7a59d0ab5314d9f504'),
(47, 'sridhar', 'drnsridhara@gmail.com', 'Bengaluru', 'fortis', NULL, NULL, '2021-05-18 19:09:02', '2021-05-18 19:09:02', '2021-05-18 20:39:02', 1, 'Abbott', '0f981bc61d17794aaa79b212bde50550'),
(48, 'Prakash Jana', 'prakash.jana1@gmail.com', 'Bhubaneswar', 'Care Hospital', NULL, NULL, '2021-05-18 19:09:10', '2021-05-18 19:09:10', '2021-05-18 20:39:10', 1, 'Abbott', '6cfa56288891e6e2665a00a068d8d36e'),
(49, 'Ramdas Nayak ', 'ramdasnayakh@gmail.com', 'Kochi ', 'Rajagiri ', NULL, NULL, '2021-05-18 19:09:17', '2021-05-18 19:09:17', '2021-05-18 20:39:17', 1, 'Abbott', '0fa56e35f6bf2a123a12721fab7c9a91'),
(50, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru ', 'Bengaluru ', NULL, NULL, '2021-05-18 19:09:43', '2021-05-18 19:09:43', '2021-05-18 19:20:21', 0, 'Abbott', '8b33dcb266c7fdc5e28ff668bf519718'),
(51, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:10:05', '2021-05-18 19:10:05', '2021-05-18 20:40:05', 1, 'Abbott', '3a7944a2705fb77d32c6eae57959fd44'),
(52, 'Sridhara G', 'sridharag@hottmail.com', 'Bangalore ', 'VIKRAM ', NULL, NULL, '2021-05-18 19:10:18', '2021-05-18 19:10:18', '2021-05-18 20:40:18', 1, 'Abbott', '766fb0a26030e3937f9ca54b128bb4ef'),
(53, 'Prakash Jana', 'prakash.jana1@gmail.com', 'Bhubaneswar', 'Care Hospital', NULL, NULL, '2021-05-18 19:12:14', '2021-05-18 19:12:14', '2021-05-18 20:42:14', 1, 'Abbott', '09f53006689aeb5d5a51158077b3e2ce'),
(54, 'Satyaranjan  Mohanty', 'satya.bapun@gmail.com', 'BHUBANESWAR', 'Apollo Hospitals ', NULL, NULL, '2021-05-18 19:12:22', '2021-05-18 19:12:22', '2021-05-18 20:42:22', 1, 'Abbott', 'ea0a1138f8a0bb5ee7c695d6abf85232'),
(55, 'Yashwanth Lakshmaiah', 'yashii.27@gmail.com', 'Kolar', 'Narayana Hrudayalaya ', NULL, NULL, '2021-05-18 19:13:25', '2021-05-18 19:13:25', '2021-05-18 20:43:25', 1, 'Abbott', 'ff8694180045754c33d0ad63ab54589c'),
(56, 'SARATH SK', 'sarathsksjm@gmail.com', 'THIRUVANANTHAPURAM', 'Abbott', NULL, NULL, '2021-05-18 19:14:11', '2021-05-18 19:14:11', '2021-05-18 20:44:11', 1, 'Abbott', '9e54d64d1e98230abc1a36485db5d8f6'),
(57, 'SARATH SK', 'sarath.krishnadas@abbott.com', 'THIRUVANANTHAPURAM', 'Abbott', NULL, NULL, '2021-05-18 19:15:05', '2021-05-18 19:15:05', '2021-05-18 20:45:05', 1, 'Abbott', '211e7f6f7b2a3378bc90db8b3833a24b'),
(58, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-05-18 19:15:39', '2021-05-18 19:15:39', '2021-05-18 19:16:24', 0, 'Abbott', '2e1eb6880ab95b503e75f3452f30ad04'),
(59, 'Sagar Mhadgut ', 'sagar.mhadgut@abbott.com', 'Mumbai', 'Abbott ', NULL, NULL, '2021-05-18 19:17:59', '2021-05-18 19:17:59', '2021-05-18 20:47:59', 1, 'Abbott', 'e2c1036d20fc0c0905a8d63a10bd5c91'),
(60, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-18 19:18:41', '2021-05-18 19:18:41', '2021-05-18 20:48:41', 1, 'Abbott', '7312c52384d0024d3ad3dd741807b746'),
(61, 'Ramkumar', 'dr.ramkumars.md@gmail.com', 'Bangalore', 'MSRNH', NULL, NULL, '2021-05-18 19:18:42', '2021-05-18 19:18:42', '2021-05-18 20:48:42', 1, 'Abbott', '5ecc502cff77cb57328d986a3fa1dabf'),
(62, 'Prabhavathi ', 'prabhavathi_in@yahoo.com', 'Bangalore', 'Jayadeva ', NULL, NULL, '2021-05-18 19:19:01', '2021-05-18 19:19:01', '2021-05-18 20:49:01', 1, 'Abbott', '136523f4a5dec85dbc791f405ab150b3'),
(63, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-18 19:19:04', '2021-05-18 19:19:04', '2021-05-18 20:49:04', 1, 'Abbott', 'f35e7af48aef22a8feeab84b0ec7aca0'),
(64, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-18 19:20:46', '2021-05-18 19:20:46', '2021-05-18 20:50:46', 1, 'Abbott', '8fd9a1f96e6fc8fcdbca16b5ddd84513'),
(65, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru ', 'Manipal ', NULL, NULL, '2021-05-18 19:20:54', '2021-05-18 19:20:54', '2021-05-18 19:29:51', 0, 'Abbott', 'e8a23e434aeb2fe47d904f7dc11d982a'),
(66, 'DR ARTI GOVINDLAL BAHETI ', 'drartibaheti@gmail.com', 'Banglore ', 'Fortis', NULL, NULL, '2021-05-18 19:22:07', '2021-05-18 19:22:07', '2021-05-18 19:57:36', 0, 'Abbott', '8f5ffae23d889b9d3477fede1887c87d'),
(67, 'Susanta Pradhan ', 'susantapradhan77@gmail.com', 'Bhubaneswar ', 'Utkal Hospital ', NULL, NULL, '2021-05-18 19:24:20', '2021-05-18 19:24:20', '2021-05-18 20:54:20', 1, 'Abbott', '31ceb1a94955d1d3570c0d7d0cf0d033'),
(68, 'Ram', 'dr.ramkumars.md@gmail.com', 'Blore', 'Msrnh', NULL, NULL, '2021-05-18 19:24:46', '2021-05-18 19:24:46', '2021-05-18 20:54:46', 1, 'Abbott', '8cf8be1663039dcb9f38bab81e21db59'),
(69, 'Padmakumar ', 'padmakumar69@yahoo.co.in', 'Manipal ', 'KMC', NULL, NULL, '2021-05-18 19:24:48', '2021-05-18 19:24:48', '2021-05-18 20:54:48', 1, 'Abbott', '95ed4d0d57f28d49a9db17154d20ca1b'),
(70, 'Abhijit Kulkarni', 'abhijitvilaskulkarni@yahoo.com', 'Bannerghatta Road Bangalore', 'Apollo ', NULL, NULL, '2021-05-18 19:26:43', '2021-05-18 19:26:43', '2021-05-18 20:56:43', 1, 'Abbott', '1b96b602e811535155ea621fa447993e'),
(71, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'MPL', 'Kmc', NULL, NULL, '2021-05-18 19:27:53', '2021-05-18 19:27:53', '2021-05-18 20:57:53', 1, 'Abbott', 'f30a6f5c9025d3fd519f064fc9f8857b'),
(72, 'Dr Eshwari Devi', 'eshwaridevi.cardiology@gmail.com', 'Bangalore ', 'Columbia Asia Referral Hospital Yeshwanthpur ', NULL, NULL, '2021-05-18 19:27:58', '2021-05-18 19:27:58', '2021-05-18 20:57:58', 1, 'Abbott', '780a8cc670a9fd26c654d6a5ecdb83b2'),
(73, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021-05-18 19:28:13', '2021-05-18 19:28:13', '2021-05-18 20:58:13', 1, 'Abbott', 'a03ffcdefe5354a8b81905ad2499f536'),
(74, 'K r Shyamsunder ', 'krs.sunder@gmail.com', 'Bangalore ', 'Sagar Hosp ', NULL, NULL, '2021-05-18 19:29:31', '2021-05-18 19:29:31', '2021-05-18 20:59:31', 1, 'Abbott', 'd6b1e7c4730a971e9e60320959774c71'),
(75, 'Biswaranjan Jena', 'drbrjena@gmail.com', 'Cuttack ', 'Ashwani ', NULL, NULL, '2021-05-18 19:29:33', '2021-05-18 19:29:33', '2021-05-18 20:59:33', 1, 'Abbott', 'ff6c49df11dc09828f2a046e58468352'),
(76, 'Sridhar L', 'srisrisastry@gmail.com', 'Bangalore', 'Sri Jayadeva Institute of Cardiovascular Sciences & Research', NULL, NULL, '2021-05-18 19:29:48', '2021-05-18 19:29:48', '2021-05-18 20:59:48', 1, 'Abbott', '87f2d5d1326ec9c4e637658a26fb1988'),
(77, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru ', 'Manipal ', NULL, NULL, '2021-05-18 19:30:21', '2021-05-18 19:30:21', '2021-05-18 21:00:21', 1, 'Abbott', '3d76a4139a2a41a454b0ca51aed19901'),
(78, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:30:33', '2021-05-18 19:30:33', '2021-05-18 21:00:33', 1, 'Abbott', 'ab37243702e2bfd0561f92496498f4d4'),
(79, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:32:16', '2021-05-18 19:32:16', '2021-05-18 21:02:16', 1, 'Abbott', '910c8e6885908a01837ec5006f9ba2c7'),
(80, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:33:35', '2021-05-18 19:33:35', '2021-05-18 21:03:35', 1, 'Abbott', '7362b84a35348113c0ef413e335ee43f'),
(81, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:34:25', '2021-05-18 19:34:25', '2021-05-18 21:04:25', 1, 'Abbott', '3e1dc7f0427c4121b0b758e764833e66'),
(82, 'Sridhar L', 'srisrisastry@gmail.com', 'Bangalore', 'Sri Jayadeva Institute of Cardiovascular Sciences & Research', NULL, NULL, '2021-05-18 19:34:41', '2021-05-18 19:34:41', '2021-05-18 20:04:52', 0, 'Abbott', '08e9e0ef91a91955f521dec4b2c4ff2d'),
(83, 'sagar desai', 'desaisagar13@gmail.com', 'Bagalkot', 'H S k', NULL, NULL, '2021-05-18 19:37:10', '2021-05-18 19:37:10', '2021-05-18 21:07:10', 1, 'Abbott', '1c15f435c9079b9f3c0428c16cd5c47f'),
(84, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:39:24', '2021-05-18 19:39:24', '2021-05-18 21:09:24', 1, 'Abbott', '9b5322834eab2dda0a4325a7f1f59339'),
(85, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-18 19:39:37', '2021-05-18 19:39:37', '2021-05-18 21:09:37', 1, 'Abbott', '84d8508a40353f7f6361f7c7d66c8657'),
(86, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:39:47', '2021-05-18 19:39:47', '2021-05-18 21:09:47', 1, 'Abbott', 'acfd4d3d5c506c25bcba4cc577631c12'),
(87, 'Dr Srinidhi S Hegde', 'dr.srihegde@gmail.com', 'Mysuru', 'SJICR', NULL, NULL, '2021-05-18 19:40:31', '2021-05-18 19:40:31', '2021-05-18 21:10:31', 1, 'Abbott', '871b8c214cef471f768eed6a59fb850e'),
(88, 'Deepak Davidson', 'deepakdavidson@yahoo.com', 'Kottayam', 'Caritas Hospital', NULL, NULL, '2021-05-18 19:40:41', '2021-05-18 19:40:41', '2021-05-18 21:10:41', 1, 'Abbott', 'c58d1fa2fb11807c623c033c9812c81c'),
(89, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:40:47', '2021-05-18 19:40:47', '2021-05-18 21:10:47', 1, 'Abbott', 'ae4a75ed9c694afa510e16404c1fa3f9'),
(90, 'sathya narayana', 'drsathyanh@gmail.com', 'BANGALORE', 'NARAYANA HRUDALAYA', NULL, NULL, '2021-05-18 19:40:57', '2021-05-18 19:40:57', '2021-05-18 19:54:42', 0, 'Abbott', 'f15d0060bff500118d0260b45f1a951d'),
(91, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:42:31', '2021-05-18 19:42:31', '2021-05-18 21:12:31', 1, 'Abbott', '6dd75528ca6b5ac19479b4e9f1145d36'),
(92, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:43:06', '2021-05-18 19:43:06', '2021-05-18 21:13:06', 1, 'Abbott', '915c26784b8f39a4091ed24854ca2d2f'),
(93, 'Abhi', 'abhipmch08@gmail.com', 'Bengaluru', 'NH', NULL, NULL, '2021-05-18 19:44:01', '2021-05-18 19:44:01', '2021-05-18 19:49:44', 0, 'Abbott', '0afb1754011201b97ede6521fda7a6e5'),
(94, 'Praveen', 'pjshetty1974@rediffmail.com', 'Mangalore', 'A J Hospital', NULL, NULL, '2021-05-18 19:44:20', '2021-05-18 19:44:20', '2021-05-18 21:14:20', 1, 'Abbott', '708487ee8d2366b9ed51457b00dd6310'),
(95, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-05-18 19:44:31', '2021-05-18 19:44:31', '2021-05-18 21:14:31', 1, 'Abbott', '547f3def7f1d12dd761d37f2d89e62d6'),
(96, 'Dr ravi', 'drrsvicardio@yahoo.co.in', 'Bangalore', 'Bgs', NULL, NULL, '2021-05-18 19:45:22', '2021-05-18 19:45:22', '2021-05-18 21:15:22', 1, 'Abbott', '96bc9a1f28e5de715d156c7837c79ad0'),
(97, 'Mahendra', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-18 19:46:00', '2021-05-18 19:46:00', '2021-05-18 21:16:00', 1, 'Abbott', 'b7eef81afeda74cbd94bfd3c363572e3'),
(98, 'Mahendra', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-18 19:46:03', '2021-05-18 19:46:03', '2021-05-18 21:16:03', 1, 'Abbott', '405cd9a373daa4950c2428cb0dadc778'),
(99, 'Madakari', '2020madhu.icare@gmail.com', 'Bengaluru', 'Sparsh', NULL, NULL, '2021-05-18 19:46:37', '2021-05-18 19:46:37', '2021-05-18 21:16:37', 1, 'Abbott', '1a2b8e493dbd7de420f4505719c4fa9e'),
(100, 'Mahendra', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-18 19:46:51', '2021-05-18 19:46:51', '2021-05-18 21:16:51', 1, 'Abbott', 'a0c5bea0723b75fd0cca8d4a5369d19e'),
(101, 'Dr.Anand N.B', 'anandnb55@gmail.com', 'Mysore ', 'Apollo Hospital ', NULL, NULL, '2021-05-18 19:47:04', '2021-05-18 19:47:04', '2021-05-18 19:47:47', 0, 'Abbott', '9ab72f56f09c78bf796c05c45126e83c'),
(102, 'Mahendra', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-05-18 19:47:14', '2021-05-18 19:47:14', '2021-05-18 21:17:14', 1, 'Abbott', '7910db04e05f54b3cd51927beec5fed9'),
(103, 'SRIDHARA ', 'sridharag@hotmail.com', 'Bangalore ', 'Vikram', NULL, NULL, '2021-05-18 19:50:24', '2021-05-18 19:50:24', '2021-05-18 21:20:24', 1, 'Abbott', '3cc002f3df8b2fb39b35a284791fc487'),
(104, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Hosur', 'ABBOTT', NULL, NULL, '2021-05-18 19:53:36', '2021-05-18 19:53:36', '2021-05-18 21:23:36', 1, 'Abbott', 'f5175202a260513ac2dd3161f69e06e5'),
(105, 'Dr ravi', 'drravicardio@yahoo.co.in', 'Bangalore', 'Bgs', NULL, NULL, '2021-05-18 19:54:39', '2021-05-18 19:54:39', '2021-05-18 21:24:39', 1, 'Abbott', 'e02fa7742c7f4c84ab52f81e6986a885'),
(106, 'Sridhar L', 'srisrisastry@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-18 19:54:54', '2021-05-18 19:54:54', '2021-05-18 21:24:54', 1, 'Abbott', 'f69c3625ca558e2a8fd9558b95aba348'),
(107, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:59:21', '2021-05-18 19:59:21', '2021-05-18 21:29:21', 1, 'Abbott', 'e8868154e2933f714ea78bc52e91a838'),
(108, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-05-18 19:59:29', '2021-05-18 19:59:29', '2021-05-18 21:29:29', 1, 'Abbott', 'a1bc663bb013f948b0b619cd5b7ed210'),
(109, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:59:30', '2021-05-18 19:59:30', '2021-05-18 21:29:30', 1, 'Abbott', 'f5b1a12e7a16351baeacccc45d08a381'),
(110, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:59:30', '2021-05-18 19:59:30', '2021-05-18 21:29:30', 1, 'Abbott', '8d139e0871d629321a90f3b2a2ef6aef'),
(111, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 19:59:30', '2021-05-18 19:59:30', '2021-05-18 21:29:30', 1, 'Abbott', '4d5b64a68fe1cb59c1dd3b85cb69c8be'),
(112, 'SHRIDHARA T R', 'drshrishridhar@gmail.com', 'Mysore', 'Cauvery heart and multispecialty hospital ', NULL, NULL, '2021-05-18 20:01:07', '2021-05-18 20:01:07', '2021-05-18 21:31:07', 1, 'Abbott', '1e80e6eec8f7e37344fe09a88a7f1db7'),
(113, 'Raju HD', 'hdraju@gmail.com', 'Mysore', 'Apollo BGS ', NULL, NULL, '2021-05-18 20:01:35', '2021-05-18 20:01:35', '2021-05-18 21:31:35', 1, 'Abbott', '6d9c86254bc223e08819ad2862541ea0'),
(114, 'PADMAJA', 'padmar091@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-05-18 20:02:31', '2021-05-18 20:02:31', '2021-05-18 21:32:31', 1, 'Abbott', '43e057d30ccdf2127bad76363ba0fe4e'),
(115, 'Lijo ', 'lijo.k@abbott.com', 'Cochin ', 'Abbott ', NULL, NULL, '2021-05-18 20:12:04', '2021-05-18 20:12:04', '2021-05-18 21:42:04', 1, 'Abbott', '05ae871593e3033cfaec86393c7cad63'),
(116, 'Praveen', 'pjshetty1974@rediffmail.com', 'Mangalore', 'A J Hospital', NULL, NULL, '2021-05-18 20:12:48', '2021-05-18 20:12:48', '2021-05-18 21:42:48', 1, 'Abbott', '2877f580216f4dd421efe3c20af1c900'),
(117, 'sreedhara R', 'kummas82@gmail.com', 'mysore ', 'sjicr ', NULL, NULL, '2021-05-18 20:14:47', '2021-05-18 20:14:47', '2021-05-18 21:44:47', 1, 'Abbott', '9feca7fde4ba6079f2e8c114b0de1af8'),
(118, 'Krishna chaitanya.M ', 'mkchaitu63@gmail.com', 'Bangalore ', 'NarayanaHrudayalaya ', NULL, NULL, '2021-05-18 20:16:04', '2021-05-18 20:16:04', '2021-05-18 21:46:04', 1, 'Abbott', '2d17358fb150434775e1c85cae3d9f25'),
(119, 'Dr Eshwari Devi', 'eshwaridevi.cardiology@gmail.com', 'Bangalore ', 'Columbia Asia Referral hospital', NULL, NULL, '2021-05-18 20:16:14', '2021-05-18 20:16:14', '2021-05-18 21:46:14', 1, 'Abbott', 'bc689bed515e8b6e5bea8c0890d15854'),
(120, 'Amal VYas', 'amal.vyas@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-18 20:20:32', '2021-05-18 20:20:32', '2021-05-18 21:50:32', 1, 'Abbott', '00c2edc9260275944ca406646d004d3d'),
(121, 'Jagadeesh', 'jagadeesh.kalathil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-05-18 20:32:55', '2021-05-18 20:32:55', '2021-05-18 22:02:55', 1, 'Abbott', 'd3df015464bc4537a317bb14f68009a0'),
(122, 'sagar desai', 'desaisagar13@gmail.com', 'Bagalkot', 'Hsk ', NULL, NULL, '2021-05-18 20:46:30', '2021-05-18 20:46:30', '2021-05-18 22:16:30', 1, 'Abbott', '6bafe4faba1f45d024e1b8de2a1f3b27'),
(123, 'sreedhara R', 'kummas82@gmail.com', 'mysore ', 'sjicr ', NULL, NULL, '2021-05-22 21:09:51', '2021-05-22 21:09:51', '2021-05-22 22:39:51', 1, 'Abbott', 'a55853aaf3870c21e9ad94ff7de028fb');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
